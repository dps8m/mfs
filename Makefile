all : mfs mfs2 analyze fix_6601
#CC = gcc
#LD = gcc
CC = clang
LD = clang

CFLAGS  = -g -O0
CFLAGS += -std=c99 -U__STRICT_ANSI__
CFLAGS += -Wall 
CFLAGS += -Wunused-argument \
-Wunused-function \
-Wunused-label \
-Wunused-parameter \
-Wunused-value \
-Wunused-variable \
-Wunused \
-Wextra

mfs: mfs.c mfs.h mfslib.c mfslib.h
	$(CC) $(CFLAGS) `pkg-config fuse --cflags --libs` -o mfs mfs.c mfslib.c

mfs2: mfs2.c
	$(CC) $(CFLAGS) `pkg-config fuse --cflags --libs` -o mfs2 mfs2.c

analyze : analyze.c

fix_6601 : fix_6601.c

clean :
	-rm analyze mfs fix_6601
