// fix_6601 root_disk
//
// Changes the config deck opca model number to 6001.

// Assumes a 3381 disk.
#define sect_per_cyl 255
#define sect_per_rec 2
#define number_of_sv 3

// Assumes the conf partition is on subvolume 0.
#define sv0 0

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

typedef uint64_t word36;

#define MASK17 0000000377777
#define SIGN18 0000000400000

// .dsk files 
// data is in packed 72 format
// word72 <=> uchar[9]


#define SECTOR_SZ_IN_W36 512
#define SECTOR_SZ_IN_BYTES ((36 * SECTOR_SZ_IN_W36) / 8)
#define RECORD_SZ_IN_BYTES (2 * SECTOR_SZ_IN_BYTES)
typedef uint8_t record[RECORD_SZ_IN_BYTES];

// Offsets in record 0

#define label_part_os (8*64)

static int fd;

static int last_sect;

static struct
  {     
    int rec;
    int sv;
    record data; 
  } cache = { -1, -1, { 1024 * 0 } };

static int fixed_bin (word36 w)
  {
    int ret = (int) (w & MASK17);
    if (w & SIGN18)
      ret = - ret;
    return ret;
  }     
    
static char * str (word36 w, char buf[5])
  {
    //static char buf[5];
    buf[0] = (w >> 27) & 0377;
    buf[1] = (w >> 18) & 0377;
    buf[2] = (w >>  9) & 0377;
    buf[3] = (w >>  0) & 0377;
    buf[4] = 0;
    for (int i = 0; i < 4; i ++)
      if (! isprint (buf[i]))
        buf[i] = '?';
    return buf;
  }

// Data conversion routines
//
//  'bits' is the packed bit stream read from the simh tape
//    it is assumed to start at an even word36 address
//
//   extr36
//     extract the word36 at woffset
//

static word36 extr36 (uint8_t * bits, uint woffset)
  {
    uint isOdd = woffset % 2;
    uint dwoffset = woffset / 2;
    uint8_t * p = bits + dwoffset * 9;

    uint64_t w;
    if (isOdd)
      {
        w  = ((uint64_t) (p[4] & 0xf)) << 32;
        w |=  (uint64_t) (p[5]) << 24;
        w |=  (uint64_t) (p[6]) << 16;
        w |=  (uint64_t) (p[7]) << 8;
        w |=  (uint64_t) (p[8]);
      }
    else
      {
        w  =  (uint64_t) (p[0]) << 28;
        w |=  (uint64_t) (p[1]) << 20;
        w |=  (uint64_t) (p[2]) << 12;
        w |=  (uint64_t) (p[3]) << 4;
        w |= ((uint64_t) (p[4]) >> 4) & 0xf;
      }
    // mask shouldn't be neccessary but is robust
    return (word36) (w & 0777777777777ULL);
  }

static void put36 (word36 val, uint8_t * bits, uint woffset)
  {
    uint isOdd = woffset % 2;
    uint dwoffset = woffset / 2;
    uint8_t * p = bits + dwoffset * 9;

    if (isOdd)
      {
        p[4] &=               0xf0;
        p[4] |= (val >> 32) & 0x0f;
        p[5]  = (val >> 24) & 0xff;
        p[6]  = (val >> 16) & 0xff;
        p[7]  = (val >>  8) & 0xff;
        p[8]  = (val >>  0) & 0xff;
        //w  = ((uint64_t) (p[4] & 0xf)) << 32;
        //w |=  (uint64_t) (p[5]) << 24;
        //w |=  (uint64_t) (p[6]) << 16;
        //w |=  (uint64_t) (p[7]) << 8;
        //w |=  (uint64_t) (p[8]);
      }
    else
      {
        p[0]  = (val >> 28) & 0xff;
        p[1]  = (val >> 20) & 0xff;
        p[2]  = (val >> 12) & 0xff;
        p[3]  = (val >>  4) & 0xff;
        p[4] &=               0x0f;
        p[4] |= (val <<  4) & 0xf0;
        //w  =  (uint64_t) (p[0]) << 28;
        //w |=  (uint64_t) (p[1]) << 20;
        //w |=  (uint64_t) (p[2]) << 12;
        //w |=  (uint64_t) (p[3]) << 4;
        //w |= ((uint64_t) (p[4]) >> 4) & 0xf;
      }
    // mask shouldn't be neccessary but is robust
  }

static int r2s (int rec, int sv)
  {
    int usable = (sect_per_cyl / sect_per_rec) * sect_per_rec;
    int unusable = sect_per_cyl - usable;
    int sect = rec * sect_per_rec;
    sect = sect + (sect / usable) * unusable; 

    int sect_offset = sect % sect_per_cyl;
    sect = (sect - sect_offset) * number_of_sv + sv * sect_per_cyl +
            sect_offset;
//printf ("r2s %d -> %d\n", rec, sect);
    return sect;
  }

static void readRecord (int fd, int rec, int sv, record * data)
  {
    if (cache.rec == rec && cache.sv == sv)
      {
        memcpy (data, & cache.data, sizeof (record));
        return;
      }

    int sect = r2s (rec, sv);
    last_sect = sect;
    off_t n = lseek (fd, sect * SECTOR_SZ_IN_BYTES, SEEK_SET);
    if (n == (off_t) -1)
      {
        fprintf (stderr, "seek off of end of data\n");
        exit (1);
      }
    ssize_t r = read (fd, & cache.data, sizeof (record));
    if (r != sizeof (record))
      {
        fprintf (stderr, "read() returned wrong size\n");
        exit (1);
      }
    cache.rec = rec;
    cache.sv = sv;
    memcpy (data, & cache.data, sizeof (record));
  }

static void writeRecord (int fd, int rec, int sv, record * data)
  {
    int sect = r2s (rec, sv);
    last_sect = sect;
    off_t n = lseek (fd, sect * SECTOR_SZ_IN_BYTES, SEEK_SET);
    if (n == (off_t) -1)
      {
        fprintf (stderr, "seek off of end of data\n");
        exit (1);
      }
    ssize_t r = write (fd, data, sizeof (record));
    if (r != sizeof (record))
      {
        fprintf (stderr, "write() returned wrong size\n");
        exit (1);
      }
    cache.rec = rec;
    cache.sv = sv;
    memcpy (& cache.data, data, sizeof (record));
  }

int main (int argc, char * argv [])
  {
    if (argc != 2)
      {
        printf ("Usage:  fix_6601 root_disk_image_file\n");
        exit (1);
      }

    fd = open (argv [1], O_RDWR);
    if (fd < 0)
      {
        printf ("Couldn't open disk image\n");
        exit (1);
      }

    word36 conf_frec = 0;
    word36 conf_nrec = 0;

// Search the partition table for the conf partition

    record r0;
    readRecord (fd, 0, sv0, & r0);

    word36 w = extr36 (r0, label_part_os +   3);
    int nparts = w;
    if (nparts > 47)
       nparts = 47;
    printf ("nparts: %d\n", nparts);

    for (int i = 0; i < nparts; i ++)
      {
        int pos = i * 4 + 4;
        w = extr36 (r0, label_part_os + pos + 0);
        char buf[5];
        if (strcmp (str (w, buf), "conf") == 0)
          {
            conf_frec = extr36 (r0, label_part_os + pos + 1);
            conf_nrec = extr36 (r0, label_part_os + pos + 2);
            break;
          }
      }
    if (conf_nrec == 0)
      {
        printf ("Couldn't find conf partiton\n");
        exit (1);
      }
    printf ("frec: %ld\n", conf_frec);
    printf ("nrec: %ld\n", conf_nrec);

// Search the conf partition

    for (uint nrec = 0; nrec < conf_nrec; nrec ++)
      {
        record r;
        readRecord (fd, conf_frec + nrec, sv0, & r);
// each card is 16 words; 1024 words/record; 64 cards/record
#define wrds_crd 16
#define crds_rec 64
        for (uint cardno = 0; cardno < crds_rec; cardno ++)
          {
            char word[5];
            uint card_os = cardno * wrds_crd;
            word36 w = extr36 (r, card_os + 0);
            if (w == 0777777777777lu)
              continue;
            //printf ("%3d %s\n", nrec * crds_rec + cardno, str (w, word));
            str (w, word);
            if (strcmp (word, "prph") == 0)
              {
                //char buf[5];
                //printf ("      name %s\n", str (extr36 (r, card_os + 1), buf));
                //printf ("      iom %c\n", fixed_bin (extr36 (r, card_os + 2)) + 'A' - 1);
                //printf ("      chan %d\n", fixed_bin (extr36 (r, card_os + 3)));
                //printf ("      model %d\n", fixed_bin (extr36 (r, card_os + 4)));
                //printf ("      state %s\n", str (extr36 (r, card_os + 5), buf));
                char name[5];
                str (extr36 (r, card_os + 1), name);
                if (strcmp (name, "opca") != 0)
                  continue;
                int model = fixed_bin (extr36 (r, card_os + 4));
                if (model == 6001)
                  { printf ("opca is already a 6001; quitting\n");
                    exit (0);
                  }
                if (model != 6601)
                  {
                    printf ("Don't know about model %d; quitting\n", model);
                    exit (1);
                  }
// Patch
                put36 (6001, r, card_os + 4);
                writeRecord (fd, conf_frec + nrec, sv0, & r);
                goto done;
              }
          }
      }
    printf ("Unable to find opca card; quitting\n");
    exit (1);
done:
    printf ("Patched opca to 6001.\n");
    return 0;
  }
